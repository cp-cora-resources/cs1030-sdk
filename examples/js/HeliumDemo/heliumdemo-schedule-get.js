#! /usr/bin/env node
/**
 *  Name:  heliumdemo-schedule-get.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
var tools = require("./tools");
var Schedule = require("cs1030_pb/scheduleTypes_pb");
var Common = require("cs1030_pb/common_pb");

tools.addCommonOptions(program);
program
  .description("Gets current schedule or a specific schedule item", {
    id: "Optional schedule item identifier (1 through 8)"    
  })
  .arguments("[id]");
  
program.action(async (id) => {
  tools.logHeader(program);
  console.log("");
 
  var bytes = null;
  var port;
  if(id != null){
    var msg = new Schedule.ScheduleItemId();
    msg.setId(0);
    msg.setIditem(Math.floor(id));  //set specific iditem
    port = 0x1C;
    bytes = msg.serializeBinary();        
  }else{
    //get everything
    var msg = new Common.Uint32Param();
    msg.setParam(0);
    port = 0x1A;
    bytes = msg.serializeBinary();
  }

  //Fire the downlink Message.node
  var result = await tools.downlink(program, program.confirmed, port, bytes);
  if (typeof result == "string") {
    console.log(result);
  } else {
    console.log(JSON.stringify(result));
  }
});
program.parse(process.argv);
