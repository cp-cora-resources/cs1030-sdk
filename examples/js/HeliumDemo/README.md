# Cora CS1030 Helium Demo

Node JS based command line tool to set time and schedule for devices connected to the Helium LoRaWAN netork. Can generate simulated downlinks or connect to helium via HTTP.
## Installation and Setup

### Install Node.js  

The tools require Node.js (javascript) tools to install your system.   If not 
already installed, please find the package for your 
system an install it. Go to https://nodejs.org/en/download/ and select your OS.
Follow the instructions there.

### Installation

1. Install the protobuf code-generated files 
   ```
   cd ../../../protobuf-cg
   npm install
   ```

2. Install the package
    ```
    npm install
    ```

## Usage

### 1. Simulation

The following version simulates a downlink generating an HTTP body that can be copied into your favorite web service client tool.

**Set the device time and location**

```node heliumdemo-timeset -- 47.697 -122.172```


**Add Schedule to turn on at Sunrise**

```heliumdemo-schedule-setweekly 3 sunrise  on 1111111```



### 2. HTTP Downlink

HTTP Downlinks requires that you have the helium HTTP integration ID and downlink key, as well as the device UUID you are sending the downlink command to.  Here's are example commands that sends the downlink directly to helium.

**Set the device time and location**

```node heliumdemo-timeset -K [downlink key] -I [integration UUID] -D [device UUID] -- 47.697 -122.172```


**Add Schedule to turn on at Sunrise**

```heliumdemo-schedule-setweekly -K [downlink key] -I [integration UUID] -D [device UUID] -- 3 sunrise  on 1111111```



---
*Copyright &copy; 2022 Codepoint Technologies, Inc.*<br/>
*All Rights Reserved*

