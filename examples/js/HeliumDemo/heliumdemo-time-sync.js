#! /usr/bin/env node
/**
 *  Name:  heliumdemo-time-sync.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
var tools = require("./tools");
var common_pb = require("cs1030_pb/common_pb");

tools.addCommonOptions(program);
program
  .description("Downlinks TimeSync message")
  .option("-r, --request", "Request device initiate time sync", false)
  .option(
    "-d, --devicetime <time>",
    "Time reported by device in last time sync uplink. Units are seconds since midnight 1970"
  )
  .option(
    "-s, --servertime [time]",
    "Time at server when TimeSync was received from the device.  If option is specified with no time value, then current time is used. Units are seconds since midnight 1970"
  )
  .option(
    "-c, --correction <value>",
    "Optional correction in seconds to apply."
  );

program.action(async () => {
  tools.logHeader(program);
  console.log("");

  const time = Math.floor(new Date().getTime() / 1000);

  //Construct the TimeSync message.
  var tsync = new common_pb.TimeSync();

  //Request
  if (program.request !== undefined) {
    tsync.setRequest(program.request);
  }

  //Device Time
  if (program.devicetime !== undefined) {
    tsync.setTimedevice(program.devicetime);
  }

  //Server Time
  if (program.servertime !== undefined) {
    if(program.servertime === true){      
      tsync.setTimeserver(time);
    }else{
      tsync.setTimeserver(program.servertime);
    }
  }

  //Correction
  if (program.correction !== undefined) {
    tsync.setTimecorrection(program.correction);
  }

  //Serialize
  var bytes = tsync.serializeBinary();

  //Fire the downlink Message.
  var result = await tools.downlink(program, program.confirmed, 0x16, bytes);
  if (typeof result == "string") {
    console.log(result);
  } else {
    console.log(JSON.stringify(result));
  }
});
program.parse(process.argv);
