#! /usr/bin/env node
/**
 *  Name:  tools.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const util = require("util");
const helium = require("./heliumhttp");

/*
 * * Recursively merge properties of two objects
 * */
function _mergeObjects(obj1, obj2) {
  for (let p in obj2) {
    try {
      // Property in destination object set; update its value.
      if (obj2[p].constructor == Object) {
        obj1[p] = _mergeObjects(obj1[p], obj2[p]);
      } else {
        obj1[p] = obj2[p];
      }
    } catch (e) {
      // Property in destination object not set; create it and set its value.
      obj1[p] = obj2[p];
    }
  }
  return obj1;
}

function _addCommonOptions(program) {
  program.option(
    "-D, --device <devuuid>",
    "Helium Device UUID. Required if performing the downlink."
  );
  program.option(
    "-I, --idinteg <idintegration>",
    "Helium Integration Identifier overrides HELIUMDEMO_IDINTEGRATION environment variable."
  );
  program.option(
    "-K, --key <downlinkkey>",
    "Helium downlink key overrides HELIUMDEMO_DOWNLINKKEY environment variable."
  );
  program.option(
    "-U, --url <url>",
    'Overrides default or "HELIUMDEMO_URL" environment variable.'
  );
  program.option("-v, --verbose", "Verbose output.", false);
}

function _stripQuotes(str) {
  if (str == null || str.length == 0) return null;
  else return str.replace(/^"|"$/g, "");
}

function _getDeviceUuid(program) {
  if (program.device === undefined) {
    if (
      process.env.HELIUMDEMO_DEVUUID === undefined ||
      process.env.HELIUMDEMO_DEVUUID.length == 0
    )
      throw new Error("Helium integration identifier not defined");
    else return _stripQuotes(process.env.HELIUMDEMO_DEVUUID);
  } else return program.device;
}

function _getIdIntegration(program) {
  if (program.idinteg === undefined) {
    if (
      process.env.HELIUMDEMO_IDINTEGRATION === undefined ||
      process.env.HELIUMDEMO_IDINTEGRATION.length == 0
    )
      throw new Error("Helium integration identifier not defined");
    else return _stripQuotes(process.env.HELIUMDEMO_IDINTEGRATION);
  } else return program.idinteg;
}

function _getDownlinkKey(program) {
  if (program.key === undefined) {
    if (
      process.env.HELIUMDEMO_DOWNLINKKEY === undefined ||
      process.env.HELIUMDEMO_DOWNLINKKEY.length == 0
    )
      throw new Error("Helium downlink key not defined");
    else return _stripQuotes(process.env.HELIUMDEMO_DOWNLINKKEY);
  } else return program.key;
}

function _logHeader(program) {
  if (program.verbose === true) {
    console.log("");
    console.log("DEVICE UUID:     " + _getDeviceUuid(program));
    console.log("ID INTEGRATION:  " + _getIdIntegration(program));
    console.log("DOWNLINK KEY:    " + _getDownlinkKey(program));
  }
}

/**
 * Asynchronous function downlinks a message to the device.
 * @param {boolean} confirmed True, message will be confirmed.
 * @param {number}  port Port number to downlink.
 * @param {Buffer|Uint8Array}  payload Buffer containing downlink payload.
 * @returns {Promise} Returns promise for the downlink connection.
 */
function _Downlink(program, confirmed, port, payload) {  
  if(payload instanceof Uint8Array)
  {
    payload = Buffer.from(payload);
  }

  //Helium does not forward 0 length buffers to end-point, force at least 1 byte in "empty port" downlinks
  if(payload.length == 0){
    payload = Buffer.from([0]);
  }

  const bsimulate = program.device == undefined || program.idinteg == undefined
     || program.key == undefined;

  if (bsimulate) {
    return new Promise((resolve) => {
      resolve(helium.prepareHttpBody(confirmed, port, payload, 2));
    });
  } else {
    return helium.downlinkHttp(
      _getIdIntegration(program),
      _getDownlinkKey(program),
      _getDeviceUuid(program),
      confirmed,
      port,
      payload
    );
  }
}

module.exports = {
  _getIdIntegration: _getIdIntegration,
  _getDownlinkKey: _getDownlinkKey,
  logHeader: _logHeader,
  mergeObjects: _mergeObjects,
  addCommonOptions: _addCommonOptions,
  stripQuotes: _stripQuotes,
  downlink: _Downlink,
};
