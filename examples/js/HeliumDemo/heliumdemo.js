#! /usr/bin/env node
/**
 *  Name:  heliumdemo.h
 *  
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const {program } = require('commander');

program
    .version('1.0.0')
    .command('heliumdemo', 'Helium Demo commands')
    .command('heliumdemo-timeset', 'Set device time')
    .command('heliumdemo-schedule', 'Schedule management commands')
    .command('heliumdemo-schedule-setweekly', 'Set a weekly schedule item.')
    .parse( process.argv);

    