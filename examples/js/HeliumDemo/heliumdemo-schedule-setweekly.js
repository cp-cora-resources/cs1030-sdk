#! /usr/bin/env node
/**
 *  Name:  heliumdemo-schedule-setweekly.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
var tools = require("./tools");
var Schedule = require("cs1030_pb/scheduleTypes_pb");

tools.addCommonOptions(program);
program
  .description("Adds/updates a weekly schedule item.", {
    id: "Schedule item identifier (1 through 8)",
    trigger:
      'How the the schedule is trigger:\n "time" - time of day (seconds)\n "sunrise" - triggers at sunrise.\n  "sunset" - triggers at sunset.\n',
    action: 'action: "on", "off", "toggle"',
    mask: "Day bit mask:  1111111 (all days), 0000001 (sunday), 1000000 (saturday), 0111110 (weekdays), 1000001 (weekend).",
  })
  .arguments("<id> <trigger> <action <mask>")
  .option(
    "-o, --offset <offset>",
    "Optional offset to initiate the trigger in seconds."
  )
  .option(
    "-c, --current",
    "Optional use current time as seconds in day"
  )
  .option(
    "-s, --secday <seconds>",
    'Optional Specifies seconds in day to trigger. This is seconds since midnight in timebase specified when setting time on the device.' +
    'For example, default time zone is GMT thus to determine seconds in day for PST <sec since midnight> - <timezone offset * 3600>'
  )
  .option("-l, --link <linkid>", "Optional linked schedule item identifer. ");

program.action(async (id, trigger, action, mask) => {
  tools.logHeader(program);
  console.log("");

  //Convert to trigger type.
  var ttype;
  switch (trigger.toLowerCase()) {
    case "sunrise":
      ttype = Schedule.TriggerType.TRIGGER_SUNRISE;
      break;
    case "sunset":
      ttype = Schedule.TriggerType.TRIGGER_SUNSET;
      break;
    default:
      ttype = Schedule.TriggerType.TRIGGER_EPOCH;
      break;
  }

  //Convert to action.
  var iAction;
  switch (action.toLowerCase()) {
    case "on":
      iAction = Schedule.Action.ACTION_SET;
      break;
    case "off":
      iAction = Schedule.Action.ACTION_UNSET;
      break;
    default:
      iAction = Schedule.Action.ACTION_TOGGLE;
      break;
  }

  const imask = parseInt(mask, 2);

  //Set the weekly configuration
  var we = new Schedule.WeeklyConfig();
  we.setType(ttype);
  we.setMask(imask);
  if (program.offset !== undefined) {
    we.setOffset(Math.floor(program.offset));
  }

  if (ttype == Schedule.TriggerType.TRIGGER_EPOCH) {
    if (program.secday !== undefined) {
      we.setSecday(Math.floor(program.secday));
    } else if(program.current !== undefined){
      const dt = new Date();
      const seconds = dt.getSeconds() + (60 *(dt.getUTCMinutes())) + (3600 * (dt.getUTCHours()));      
      we.setSecday(seconds);      
    } else {
      console.error("-s, --secday option is required when using trigger epoch");
      return;
    }
  }

  //Configure the Schedule item.
  var item = new Schedule.ScheduleItem();
  item.setWeekly(we);
  item.setId( Math.floor(id));  //Schedule Item ID
  item.setEnabled(true);
  item.setType(Schedule.ItemType.ITEMTYPE_WEEKLY);
  item.setActionstart(iAction);
  if (program.link !== undefined) {
    item.setLinkedItem(program.link);
  }

  //Define the schedule configuration.
  var cfg = new Schedule.Configuration();
  cfg.addItems(item);
  cfg.setId(0);  //Scheduler ID 0
  var bytes = cfg.serializeBinary();

  //Fire the downlink Message.
  var result = await tools.downlink(program, program.confirmed, 0x19, bytes);
  if (typeof result == "string") {
    console.log(result);
  } else {
    console.log(JSON.stringify(result));
  }
});
program.parse(process.argv);
