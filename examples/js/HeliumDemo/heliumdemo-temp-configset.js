#! /usr/bin/env node
/**
 *  Name:  heliumdemo-temp-configset.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
var tools = require("./tools");
var temp_pb = require("cs1030_pb/temperature_pb");

tools.addCommonOptions(program);
program
  .description("Sets CS1030 device temperature configuration.", {
    id: "Unique Trigger Identifier 1-n",
    threshold: "Trigger Threshold (C)",    
    mode: "Trigger Mode: absgt, abslt, delta, deltapos, deltaneg, rate, ratepos, rateneg",
  })  
  .option(
    "-i, --interval <seconds>",
    "Sampling Interval in seconds (0 = disabled).  Required for triggers and statistics." 
  )
  .option(
    "-r, --report <minutes>",
    "Temperature reporting interval in minutes (0 = disabled)"
  )  
  .option(
    "-s, --stats <minutes>",
    "Statistics reporting interval in minutes (0 = disabled)"
  )  
  .option(
    "--cnotify",
    "Confirm trigger and report uplinks.",
    true
  )  
  .option(
    "--cstats",
    "Confirm statistics uplinks.",
    false
  )  

program.action(async (id, threshold, mode) => {
  tools.logHeader(program);
  console.log("");

  var config = new temp_pb.Configuration();
  config.setId(0);
  if(program.interval != undefined){
    config.setSamplinginterval(program.interval);
  }
  if(program.report != undefined){
    config.setReportinterval(program.report);
  }
  if(program.stats != undefined){
    config.setStatsreportinterval(program.stats);
  }
  config.setConfirmtrigger(program.cnotify);
  config.setConfirmstats(program.cstats);

  var bytes = config.serializeBinary();

  //Fire the downlink Message.
  var result = await tools.downlink(program, program.confirmed, 0x34, bytes);
  if (typeof result == "string") {
    console.log(result);
  } else {
    console.log(JSON.stringify(result));
  }
});
program.parse(process.argv);
