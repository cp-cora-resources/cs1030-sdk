#! /usr/bin/env node
/**
 *  Name:  heliumdemo-time-configset.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
var tools = require("./tools");
var common_pb = require("cs1030_pb/common_pb");

tools.addCommonOptions(program);
program
  .description("Sets CS1030 device time configuration.", {
    lat: "Device latitude in degrees",
    lon: "Device longitude in degrees",
  })
  .arguments("<lat> <lon>")
  .option(
    "-c, --confirmed",
    "Downlink message is confirmed when defined.",
    false
  )
  .option(
    "-t, --timezone <zone>",
    "Specify the device time zone,  not necessary when  latitude and longitude are provided."
  )

  .option("-d, --daylight", "Daylight savings enabled.", false);

program.action(async (lat, lon) => {
  tools.logHeader(program);
  console.log("");

  //Construct the configuration message.
  const ilat = Math.round(lat * 100.0);
  const ilon = Math.round(lon * 100.0);

  var tcfg = new common_pb.TimeConfiguration();

  if (program.timezone !== undefined) {
    tcfg.setTimezone(program.timezone);
  }

  tcfg.setLatitude(ilat);
  tcfg.setLongitude(ilon);
  tcfg.setBdaylightsavings(program.daylight);
  var bytes = tcfg.serializeBinary();

  //Fire the downlink Message.
  var result = await tools.downlink(program, program.confirmed, 0x14, bytes);
  if (typeof result == "string") {
    console.log(result);
  } else {
    console.log(JSON.stringify(result));
  }
});
program.parse(process.argv);
