"use strict";
/**
 *  Name:  downlinkHttp.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const bent = require("bent");
/**
 * Asynchronous function downlinks a message to the device.
 * @param {string} idIntegration  Integration Identifier.
 * @param {string} downlinkKey Downlink access key.
 * @param {string} uuid  Device UUID.
 * @param {boolean} confirmed True, message will be confirmed.
 * @param {number}  port Port number to downlink.
 * @param {Buffer}  payload Buffer containing downlink payload.
 * @param {string}  url Helium URL default is https://console.helium.com/api/v1
 * @returns {Promise} Returns promise for the downlink connection.
 */
function downlinkHttp(
  idIntegration,
  downlinkKey,
  uuid,
  confirmed,
  port,
  payload,
  url = "https://console.helium.com/api/v1"
) {
  //Prepare the downlink and fire.
  //Construct the path including integration and helium devive UUID.
  //https://console.helium.com/api/v1/down/{Integration_ID}/{Downlink_Key}:{helium_uuid},
  let path = "/down/" + idIntegration + "/" + downlinkKey + "/" + uuid;

  let post = bent(url, "POST", "string", [200]);

  //Downlink one or more packets.
  const body = {
    confirmed: confirmed,
    payload_raw: payload.toString('base64'),
    port: parseInt(port),
  };

  console.log(JSON.stringify(body, null, 1));
  return post(path, body);
  promises.push(post(path, body));
}

/**
 *
 * @param {boolean} confirmed True, message will be confirmed.
 * @param {number}  port Port number to downlink.
 * @param {Buffer}  payload Buffer containing downlink payload.
 * @param {number} ctSpaces number of spaces in JSON return object.
 * @returns {string} Returns the JSON string ready for Helium submission.
 */
function prepareHttpBody(confirmed, port, payload, ctSpaces = 2) {
  //Downlink one or more packets.
  const body = {
    confirmed: confirmed,
    payload_raw: payload.toString('base64'),
    port: parseInt(port),
  };
  return JSON.stringify(body, null, ctSpaces);
}

module.exports = {
  downlinkHttp: downlinkHttp,
  prepareHttpBody: prepareHttpBody,
};
