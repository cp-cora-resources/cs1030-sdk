#! /usr/bin/env node
/**
 *  Name:  heliumdemo-time-confgget.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
var tools = require("./tools");
var common_pb = require("cs1030_pb/common_pb");

tools.addCommonOptions(program);
program.description("Gets a CS1030 device time settings configuration.");

program.action(async () => {
  tools.logHeader(program);
  console.log("");

  //Construct the configuration message.

  var bytes = Buffer.from([]);

  //Fire the downlink Message.
  var result = await tools.downlink(program, program.confirmed, 0x15, bytes);
  if (typeof result == "string") {
    console.log(result);
  } else {
    console.log(JSON.stringify(result));
  }
});
program.parse(process.argv);
