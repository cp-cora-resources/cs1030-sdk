#! /usr/bin/env node
/**
 *  Name:  heliumdemo-temp-triggerset.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
var tools = require("./tools");
var temp_pb = require("cs1030_pb/temperature_pb");
const TriggerMode = temp_pb.TriggerMode;

tools.addCommonOptions(program);
program
  .description("Sets CS1030 device temperature schedule trigger.", {
    id: "Unique Trigger Identifier 1-n",
    threshold: "Trigger Threshold (C)",    
    mode: "Trigger Mode: absgt, abslt, delta, deltapos, deltaneg, rate, ratepos, rateneg",
  })
  .arguments("<id> <threshold> <mode>")
  .option(
    "-h, --hysteresis",
    "Hysteresis for trigger in C" 
  )
  .option(
    "-i, --item <iditem>",
    "Specify optional schedule item id to trigger when temperature trigger is activated"
  )  

program.action(async (id, threshold, mode) => {
  tools.logHeader(program);
  console.log("");


  //Convert to action.
  var imode;
  switch (mode.toLowerCase()) {
    case "absgt":
      imode = TriggerMode.ABSGT;
      break;
    case "abslt":
      imode = TriggerMode.ABSLT;
      break;
    case "delta":
      imode = TriggerMode.DELTA;
      break;
    case "deltapos":
      imode = TriggerMode.DELTAPOS;
      break;
    case "deltaneg":
      imode = TriggerMode.DELTANEG;
      break;
    case "rate":
      imode = TriggerMode.RATE;
      break;
    case "ratepos":
      imode = TriggerMode.RATEPOS;
      break;
    case "rateneg":
      imode = TriggerMode.RATENEG;
      break;
    default:
      imode = TriggerMode.ABSGT;
      break;
  }

  var trigger = new temp_pb.TriggerSpec();

  trigger.setId(id);
  trigger.setThreshold(threshold);
  trigger.setMode(imode);

  if(program.hystereis != undefined){
    trigger.setHysteresis(program.hystereis);
  }

  if(program.item != undefined){
    var item = new temp_pb.TriggerSpec.ScheduleItem();
    item.setId(0);
    item.setIditem(program.item);
    trigger.setScheduleiitem(item);
  }

  var config = new temp_pb.Configuration();
  config.setId(0);
  config.setTriggersList([trigger]);

  var bytes = config.serializeBinary();

  //Fire the downlink Message.
  var result = await tools.downlink(program, program.confirmed, 0x34, bytes);
  if (typeof result == "string") {
    console.log(result);
  } else {
    console.log(JSON.stringify(result));
  }
});
program.parse(process.argv);
