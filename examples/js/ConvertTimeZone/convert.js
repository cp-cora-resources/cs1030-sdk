#! /usr/bin/env node
/**
 *  Name:  convert.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 *
 * Description:
 *  Converts time zone values expressed as a floating point number in hours to longitude in 100th of a degree.
 */

const console = require("console");

/**
 * Converts time zone to approximate longitude
 * @param {number} tz Time zone offset in hours relatively to GMT.  Range is +/-24 hours.   Partial hours supported.
 * @returns {number} Returns the approximate longitude corresponding to the specified hour angle in 1/100th of a
 *  degree. This is the same units expected by the TimeConfiguration protobuf message.
 */
function tz2lon(tz) {
  let days = parseInt(tz / 24.0, 10);
  tz = tz - days * 24.0;
  return parseInt((tz * 100.0 * 360.0) / 24.0, 10);
}

//Test the function
console.log("PST Longitude: " + tz2lon(-7));
console.log("PST Longitude: " + tz2lon(-31)); //Verifies day truncation correct.
console.log("IST Longitude: " + tz2lon(5.5));
console.log("IST Longitude: " + tz2lon(29.5)); //Verifies day truncation correct.
