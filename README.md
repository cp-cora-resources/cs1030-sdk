![Cora Logo](img/cora-logo.png)

# Untethered Wall Outlet SDK - CS1030-UL

Information and examples for integrating the CS1030-UL untethered LoRaWAN device.  

The CS1030 Wall Outlet Receptacle provides real=time switch control and notification reporting.   The switch implements Codepoint's standard untethered application protocol using 
Google Protocol Buffers (protobuf) to encode and decode the messages communicated between the device and host application.   The protobuf specifications can be found at
[./defs/protobuf](defs/protobuf/README.md).  Generated source code is provided for C++, java, JavaScript (JS), and Python.  Use the protoc code generator to generate packages for other languages.   

## Table of Contents
<!-- TOC start -->
- [Untethered Wall Outlet SDK - CS1030-UL](#untethered-wall-outlet-sdk---cs1030-ul)
  - [Table of Contents](#table-of-contents)
  - [Special Considerations](#special-considerations)
  - [Functional Model](#functional-model)
  - [Application Integration](#application-integration)
  - [Protobuf Communication Messages](#protobuf-communication-messages)
    - [Powered Device Settings (poweredDevice.proto)](#powered-device-settings-powereddeviceproto)
      - [Time Synchronization](#time-synchronization)
    - [Bimodal Switch (bimodal.proto)](#bimodal-switch-bimodalproto)
    - [Switch Scheduler (scheduleTypes.proto)](#switch-scheduler-scheduletypesproto)
        - [Common Schedule Item Properties](#common-schedule-item-properties)
        - [Configuring the Weekly Time of Day (item type=1)](#configuring-the-weekly-time-of-day-item-type1)
        - [Configuring Timers (item type=2)](#configuring-timers-item-type2)
        - [Configuring Trigger Sets (item type=3)](#configuring-trigger-sets-item-type3)
        - [Start Action Items(item type=4)](#start-action-itemsitem-type4)
        - [Linking Schedule Items](#linking-schedule-items)
    - [Temperature Sensor (temperature.proto)](#temperature-sensor-temperatureproto)
      - [Temperature Triggers](#temperature-triggers)
      - [Triggering Schedule Items](#triggering-schedule-items)
      - [Performance Note](#performance-note)
  - [Example Downlinks](#example-downlinks)
  - [Tools and Sample Code](#tools-and-sample-code)
    - [Sample Application Protobuf Messages](#sample-application-protobuf-messages)
  - [Integrator's Guide](#integrators-guide)
    - [Minimum Integration](#minimum-integration)
    - [Minimum Configuration Capability](#minimum-configuration-capability)
      - [Weekly Scheduling](#weekly-scheduling)
      - [Selecting a Pre-Defined Configuration](#selecting-a-pre-defined-configuration)
  - [Firmware Errata Summary](#firmware-errata-summary)
    - [#1 TimeConfiguration timezone parameter](#1-timeconfiguration-timezone-parameter)
  - [License](#license)
<!-- TOC end -->

## Special Considerations
The CS1030-UL operates as a LoRaWAN Class-C device. Class A only operation is supoprted; however, there are limitations.

- The Real Time Clock (RTC) of the CS1030-UL requires application support if scheduling features are utilized.  
- If scheduling features are not implemented by your application, the RTC may be ignored.
- As the CS1030-UL is an AC powered device, the RTC information is not maintained in the case of power failure and should be 
  verified and configured after any Powered Device Reset Event is received.  Time Set also includes optional time zone, daylight-savings, 
  and location information that is used by some scheduling features.

For networks that do not support Class C operation, the switch will not be immediately responsive to network downlink commands.  For these applications, users should set the health check message to the appropriate interval to receive timely network downlinks.  It is not recommended that health check interval be less than 15 minutes for commercial networks given the extra load it adds.   

## Functional Model

The logical interaction model for the CS1030-UL is shown below.

![Cora Untethered Protocol](img/CS1030-UL-Protocol.png)

The applications interact with the CS1030-UL using the Cora Untethered Device Protocol, which organizes interaction into functional services as defined 
in the protobuf specifications.  The services for the CS1030-UL are as follows:

- **Device Settings** – General device configuration and notification messages.
- **Bimodal Switch** – Two-State switch (Set, Unset) controller; manages the state of the switched outlet: on (set) or off (unset). Includes
  messages for configuration, statistics, and notification.  
- **Switch Scheduler** – Configurable scheduler for the switched outlet, automatically controls the switch state according to a maximum of eight (8) configured schedule items. 
- **Temperature Sensors** – Temperature sensing messages supporting configuration, events, measurements and statistics.

The bimodal switch (id=0), switch scheduler (id=0) and a temperature sensor (id=0) (+- 2-degree Celsius accuracy)  can be configured to support
a wide variety of applications. By default the device notifies when the switch is turned on (set) and off (unset) events. It will also sends a health check notification
once an hour.  For simple applications, no further configuration may be required.  

The CS1030, however, can be configured to do much more.  Using the configuration messages application providers can configure
the device to provide periodic statistics, scheduled on/off state changes based on weekly time of day, triggered timers, report temperature data, and other notifications.   

## Application Integration

The CS1030-UL implements LoRaWAN v1.0.3, which is supported by a wide variety of LoRaWAN network providers.  As shown in the diagram 
below,  the device communicates with an application via the LoRaWAN network and gateways.   Typically, the application implements
an encoder/decoder to translate the proprietary device messages into a data structure compatible with the application.   

![CS1030 Encoder / Decoder](img/CS1030-UL-Encoder.png)

For CS1030-UL devices, this can be accomplished by decoding uplinked messages or encoding downlink messages using the code generated protobuf code
to convert to/from the binary wireline encoding into message objects.   From there, the application provider can easily translate
the data into a format compatible with their system. Depending on the network provider, it may also be possible to implement the message
encoding and decoding functions at the network application server.  For example, The Things Network (TTN) has such capabilities.

The specific design and details for integrating the CS1030-UL are mostly dependent on the application architecture and out of scope for this SDK.  This
SDK provides examples on implementing encoding/decoding functions as well as providing various test cases and stimulus to accelerate
integration.

## Protobuf Communication Messages
Programmer's have a variety of language options available to process CS1030 messages.  Conceptually, each Protobuf service interaction defined in the .proto files
are mapped to a specific port on the CS1030.  While the defined interaction is not strictly RPC, it does conform with event, request/response semantics.  Where 
noted in the .proto specifications, events are outbound messages with only the request portion of the service interaction defined.  For savvy developers,
the defined interactions could be mapped easily to gRPC if desired. 

The following sub-sections define the LoRaWAN port mappings to the Protobuf interaction for each of the services implemented
in the device.

### [Powered Device Settings (poweredDevice.proto)](defs/protobuf/poweredDevice.proto)
The Powered Device service provides interactions related to general device health and configuration.  
The table below shows the LoRaWAN port mappings for 
the specified interaction.  See the protobuf file for detailed descriptions of the messages used to communicate.

| Port (Hex) | Port (Dec.) | Interaction Type | Name (Cora.Powered.Device)   | Description                           |
|:----------:|:-----------:|:----------------:|------------------|---------------------------------------|
|    0x10    |      16     |       Event      | HealthCheckEvent | Periodic health check event           |
|    0x11    |      17     |       Event      | ResetEvent       | Device reset notification event       |
|    0x12    |      18     | Request/Response | GetConfig        | Gets the current Device configuration |
|    0x13    |      19     | Request/Response | SetConfig        | Sets the device configuration          |
|    0x14    |      20     | Request/Response | SetTimeSettings  | Sets the device time related configurations & location information      |
|    0x15    |      21     | Request/Response | GetTimeSettings  | Gets the device time related configurations & location information      |
|    0x16    |      22     | Request/Response | SynchronizeTime  | Class A/ Class C Time transfer message |

#### Time Synchronization
The CS1030 has an onboard clock that should be set if scheduler features are to be used.   This sequence can be used for networks supporting Class A or Class C devices and provides the best synchronization.  To set the clock perform the following sequence:

1.  Send SynchronizeTime (port 0x16) request with the bRequest field set to true.  Only reset if time synch not requested recently (e.g. last 24 hours).
2.  The device will respond with a TimeSync message providing the device current time.
3.  If device time within tolerance exit time sync process.
4.  Otherwise if out of tolerance, the server sends back a TimeSync message with the current server time and the device time provided in step #2. Also  any additional timeCorrections can be specified, which will be applied by the device.  The timeCorrections can provide correction of systematic biases like nominal uplink transit time (e.g. 1 second).
5.  Once the device receives the message, it will apply the time correction and reset.   Prior to reset, it will uplink the corrected time if requested  by the server.  
6.  Device sends reset notification. 
7.  Go to Step 1. and repeat until within tolerance.

With this approach it should be possible to set time within a few seconds.

### [Bimodal Switch (bimodal.proto)](defs/protobuf/bimodalSwitch.proto)
The CS1030-UL uses a bimodal switch service to report and configure the switched outlet state.  It is identified
as bimodal switch #0 (id=0).  Bimodal switch events and request/response interactions will all identify the as id=0.  

The state of the switched outlet is mapped to the bimodal switch state in the following table.

| Switched Outlet State  |  Bimodal Switch State |
|:-------------:|:--------------:|
|       Off     |      Unset     |
|       On      |       Set      |

When the switched outlet is 'on', the bimodal switch object will report 'set'; when 'off', the bimodal switch object will report 'unset'. 
The table below shows the LoRaWAN port mappings for the bimodal switch object.  See the protobuf file for detailed descriptions 
of the messages used to communicate.

| Port (Hex) | Port (Dec.) | Interaction Type | Name (Cora.BimodalSwitch.Switch) | Description                            |
|:----------:|:-----------:|:----------------:|:-------------------------:|----------------------------------------|
|    0x40    |      64     |       Event      |       NotifyEvent         | Notifies of a bimodal state event.     |
|    0x41    |      65     |       Event      |    StatsReportEvent       | Stats reported at regular intervals.   |
|    0x42    |      66     | Request/Response |        GetConfig          | Gets the switch configuration. |
|    0x43    |      67     | Request/Response |        SetConfig          | Sets the switch configuration. |
|    0x44    |      68     | Request/Response |        GetStats           | Gets the current switch statistics.   |
|    0x45    |      69     | Request/Response |       StatsClear          | Clears the current switch statistics. |
|    0x46    |      70     | Request/Response |        GetState           | Gets the current switch state ('set' or 'unset').   |
|    0x47    |      71     | Request/Response |        SetState           | Sets the current switch state ('set', 'unset', 'toggle'). |


To regularly report statistics, configure the 'statsInterval' to report at the desired interval.  Maximum report interval 
is 49 days (70,560 minutes).  Additionally, notification events and stat messages can be marked as confirmed to ensure delivery.   

### [Switch Scheduler (scheduleTypes.proto)](defs/protobuf/scheduleTypes.proto)
The switch scheduling service enables the switched outlet to automatically change the switch state according to a 
downlinked set of scheduled items.  The table below shows the LoRaWAN port mappings for the Scheduler.

| Port (Hex) | Port (Dec.) | Interaction Type | Name (Cora.Schedule.Scheduler) | Description                            |
|:----------:|:-----------:|:----------------:|:----------------------:|----------------------------------------|
|    0x19    |      25     | Request/Response |       SetSchedule      | Sets one or more schedule items.       |
|    0x1A    |      26     | Request/Response |       GetSchedule      | Gets all defined schedule items.       |
|    0x1B    |      27     | Request/Response |      ResetSchedule     | Resets schedule to factory default.    |
|    0x1C    |      28     | Request/Response |    GetScheduleItem     | Gets a specified schedule item.        |
|    0x1D    |      29     | Request/Response |  TriggerScheduleItem   | Triggers a specified schedule item.    |


Up to eight (8) schedule items may be configured for the CS1030.  To support the varied number of potential use cases, the scheduler provides four schedule item types:

* **Weekly Time of Day** – change the switch state based on time of day for any day of week. 
* **Timer**  – change the switch state using a timed delay, duration, and repetition cycle.   
* **Start action** – changes the state of the switch according to the specified start action. 
* **Trigger sets**  – trigger one or more schedule items simultaneously.  Allows grouping of coordinated schedules to create complex switching.

 By default, the schedule items are configured as follows:

| **ID** | **Item Type** | **Description**                                                         |
|:------:|:-------------:|-------------------------------------------------------------------------|
|    1   |  Start Action | Toggles the switch id=0 (mapped to button single button press)                 |
|    2   |     Timer     | Turns on ('set') switch for three (3) minutes, then turns off ('unset'). Mapped to two (2) button presses in rapid succession|
|    3   |  Not Defined  |                                                                         |
|    4   |  Not Defined  |                                                                         |
|    5   |  Not Defined  |                                                                         |
|    6   |  Not Defined  |                                                                         |
|    7   |  Not Defined  |                                                                         |
|    8   |  Not Defined  |                                                                         |

All schedule items except Weekly Time of Day can be triggered using the outlet button (either one (1) or two (2) presses), remotely triggered using TriggerScheduleItem (Port 29) interaction,
included in a trigger set, or linked to any other schedule item.   

**NOTE:** To disable triggering using the button presses, either set schedule items 1 and 2 to undefined or configure them
as Weekly Time of Day items.  

In order to use the scheduler service, the device time must be set using the Powered Device interface.  When sunrise sunset triggers are needed, 
the approximate latitude and longitude of the device is required as well.  These are also 
configurable when setting the time. In cases where the location is not known, the time zone may be specified and an approximate longitude 
will be calculated.  The more accurate the location information the more accurate the sunrise and sunset triggers will be.

##### Common Schedule Item Properties
All schedule items share a common set of properties.  These are defined in the table below.

| **Name**      | **Description**                                                                     |
|:-------------:|:------------------------------------------------------------------------------------|
| _id_          | unique item identifier, valid range 1 through 8                                     |
| _enabled_     | Indicates if the item is enabled (true) or disabled (false)                         |
| _type_        | The item type identifier: 0=Unknown, 1=weekly, 2=timer, 3=triggerset, 4=startaction |
| _actionStart_ | The bimodal action when the item is triggered:  0=none, 1=unset, 2=set, 3=toggle    |
| _actionStop_  | The bimodal action when the item is triggered: 0=none, 1=unset, 2=set, 3=toggle     |
| _linkedItem_  | Optional linked specifies the switch id (0 for the CS1030) and an item identifier: 0=not defined, 1+ the linked item identifier.      |


##### Configuring the Weekly Time of Day (item type=1)
Configuring repetitive schedule times is accomplished using the weekly schedule item.   In addition to the common schedule properties, a weekly item defines the
additional properties.

| **Name** | **Description**                                                  |
|:--------:|:-----------------------------------------------------------------|
| _type_   | The time of day (ToD) trigger type: 0=time of day, 1=sunrise, 2=sunset |
| _mask_   | Day of week mask, 7 bits Sunday through Saturday.                |
| _offset_ | Start action offset (seconds) before or after triggering.        |
| _secday_ | Time of day (seconds) to trigger the start (trigger type = 0)    |

Only the start action is used for weekly schedule items.  Weekly schedule examples are shown below.

| Description                                         | Start Action | Trigger Type | Mask | Offset | SecDay |
|:----------------------------------------------------|:------------:|:------------:|:----:|:------:|:------:|
| Turn on daily at 1 hr. before sunset                 |    set(2)    |   sunset(2)  | 0x7F |  -3600 |  -NA-  |
| Turn off daily at 00:30                             |   unset(1)   |    ToD(0)    | 0x7F |    0   |  1800  |
| Turn on 16:00 Tuesday and Thursday                  |    set(2)    |    ToD(0)    | 0x14 |    0   |  57600 |
| Turn off 2 hours after sunrise Wednesday and Friday |   unset(1)   |  sunrise(1)  | 0x06 |  7200  |  -NA-  |

##### Configuring Timers (item type=2)
Timers provides the means to do repeatable sets of actions once triggered.  They can be triggered remotely, by pushbutton, or by
another schedule item.  Timers have the following properties in addition to the common schedule properties.

| **Name**     | **Description**                                                                                 |
|:------------:|:------------------------------------------------------------------------------------------------|
| _delay_      | Optional delay (seconds) from time of triggering to start action.                               |
| _duration_   | Optional duration (seconds) between start action and stop action                                |
| _cycleCount_ | Optional Number of times to repeat the stop action waiting the specified duration in between    |

The sequence when a timer is triggered is shown in the figure below given cycle count of 3.  

![CS1030 Encoder / Decoder](img/CS1030-UL-TimerItem.png)

Some example timer settings.

| Description                                         | Start Action | Stop Action | Delay | Duration | Cycle Count |
|:----------------------------------------------------|--------------|-------------|-------|----------|-------------|
| Turn on immediately and turn off in 1 hour          | set(2)       | unset(1)    | 0     | 3600     | 2           |
| Turn on every two hours 10 times                    | set(1)       | set(2)      | 0     | 7200     | 10          |
| Turn off every two hours delayed 1.5 hours 10 times | unset(1)     | unset(1)    | 5400  | 7200     | 10          |
| Turn on and toggle every 4 hours forever.           | set(1)       | toggle(3)   | 0     | 14400    | 0xFFFFFFFF  |

Examples in row #2 and #3 can work together to turn the switch on for 1.5 hours and then off for 30 minutes.  The cycle will
repeat 10 times with the switch ending in the off state.

This type of trigger is used as the default item=2 schedule item, which causes the switch to turn on for three (3) minutes and then turns off.  It can be activated
by pressing the button twice quickly.

##### Configuring Trigger Sets (item type=3)
Trigger sets provide the means to trigger multiple items simultaneously.  Up to seven (7) items may be specified in a trigger set.
The trigger is an array of schedule item ids specifying the scheduler id(0 for the CS1030) and an item identifier: 0=not defined, 1+ the linked item identifier.  
Items in the trigger set are executed immediately whenever the trigger set is triggered.

##### Start Action Items(item type=4)
Start Action schedule items perform the specified start action whenever triggered.  This type of trigger is used as the default item=1 schedule item, which
toggles the switch whenever the button is pressed once.

##### Linking Schedule Items

The scheduling service provides a capability to link schedule items together enabling a variety of triggering options.   For example, consider a configuration where 
an item (#1) defined to trigger daily at 16:00 (4 P.M.). As shown in the figure below.   This item is linked to a trigger set (#2), which is activated whenever #1 
is triggered.  

![CS1030 Encoder / Decoder](img/CS1030-Schedule-Example.png)

The trigger set in turn is configured to trigger items #3 and #4 simultaneously.   Finally, whenever #4 is triggered #5 can also be triggered.   While not a likely 
scenario, the example demonstrates the capabilities of the scheduler to form complex schedules given the available schedule types. 

### [Temperature Sensor (temperature.proto)](defs/protobuf/temperature.proto)

The CS1030-UL has a coarse resolution (~1.6 C) calibrated temperature sensor useful for monitoring significant changes in temperature.  The sensor provides functions
for interval reporting, absolute/relative triggered threshold notifications, or statistics.   The table below shows the LoRaWAN port mappings for the Temperature service interactions.  A maximum of six (6) triggers can be configured see performance note below for further limitations.


| Port (Hex) | Port (Dec.) | Interaction Type | Name (Temperature.Sensor.) | Description                               |
|:----------:|:-----------:|:----------------:|:--------------------------:|-------------------------------------------|
|    0x30    |      48     |       Event      |     TriggerNotifyEvent     | Notifies of configured trigger events.    |
|    0x31    |      49     |       Event      |         ReportEvent        | Reports temperature at regular intervals. |
|    0x32    |      50     |       Event      |      StatsReportEvent      | Statistics reported at regular intervals. |
|    0x33    |      51     | Request/Response |          GetConfig         | Gets the sensor configuration.            |
|    0x34    |      52     | Request/Response |          SetConfig         | Sets the sensor configuration.            |
|    0x35    |      53     | Request/Response |          GetStats          | Gets the temperature statistics.          |
|    0x36    |      54     | Request/Response |         StatsClear         | Clears the temperature statistics.        |

This device defines the temperature sensor with an identifier equal to 0.   

#### Temperature Triggers
The temperature sensor supports multiple trigger types, which can generate notifications or trigger schedule items (see below) when temperature exceeds user specified thresholds.  The following trigger modes are supported.

| Trigger<br/>Definition | Trigger <br/> Mode       | Description                                                                                     |
|------------------------|--------------------------|-------------------------------------------------------------------------------------------------|
| ABSGT                  | Absolute Greater Than    | Trigger when temperature exceeds threshold.                                                     |
| ABSLT                  | Absolute Less Than       | Trigger when temperature falls below threshold.                                                 |
| DELTA                  | Relative Change          | Trigger when relative temperature exceeds threshold positive or negative (not time dependent).  |
| DELTAPOS               | Positive Relative Change | Trigger when relative temperature rises more than the specified threshold (not time dependent). |
| DELTANEG               | Negative Relative Change | Trigger when relative temperature falls more than the specified threshold (not time dependent). |
| RATE                   | Rate Exceeded            | Trigger when temperature change exceeds threshold since last sample positve or negative change. |
| RATEPOS                | Positive Rate Exceeded   | Trigger when temperature rise exceeds threshold since last sample.                              |
| RATENEG                | Negative Rate Exceeded   | Trigger when temperature fall exceeds threshold since last sample                               |



#### Triggering Schedule Items
The temperature triggers can also trigger schedule items.   The temperature trigger definition provides specification of a schedule item comprising the 
scheduler ID (0 for this device) and the schedule item ID (range = 1..N schedule items). This capability allows the switch to act as a thermostat, changing the switch states when thermal thresholds are exceeded.

#### Performance Note
Configurations with more than three (3) triggers defined will exceed the minimum data rate (for U.S. it is DR1) payload size restrictions of 51 bytes.
In these cases, the GetConfig/SetConfig responses will not uplink. The only option is to get closer to a gateway so it will communicate at 
at higher data rate.   The maximum size of the configuration can be up to 85 bytes so take that into account when planning deployment.  
If low data rate deployments are required, it is recommended that a  maximum of three (3) triggers be used.   This will allow the
configuration to be packed into the 51 byte message in most cases.


## Example Downlinks

Sample downlinks for configuring the various device services are available in the postman collection found here
[CS1030-UL-TTNv3-Test.postman_collection.json](test/postman/CS1030-UL-TTNv3-Test.postman_collection.json).  Be sure to look at the documentation 
for each postman they provide additional information about the payload specification as well as the response.

The Postmans are set up to send downlinks via TTN V3.0.  The authentication and device ID will have to be updated for 
particular CS1030-UL device to send.   

To create a custom payload:

1) Generate the encoded protobuf and convert to base-64 encoding.
2) Paste into the payload field of the appropriate Postman. Recommend duplicating the Postman before modifying.
3) Send the message.  
4) Watching the TTN console, you can see the downlink waiting to go and then monitor the responses.

Similar functions are also available with other network providers.


## Tools and Sample Code

** Note regarding Helium and "port-only" / 0-byte downlinks:**
On Helium, 0 byte downlinks (i.e. port-only downlinks) are currently discarded by the network.  While this may change in the future, as of 3/2022 on Helium...any 0-byte Downlinks must be padded with at leask 1 junk byte to allow network to forward downlinks to end device.  For any "port-only" downlink examples below, be aware of this consideration.

### Sample Application Protobuf Messages
| Message<br/>| Port <br/> | Request <br/> (Downlink B64) | Response <br/>(Uplink B64) |Description                                                                                     |
|------------------------|--------------------------|--------------------------|--------------------------|-------------------------------------------------------------------------------------------------|
| Health Check Event | 16 | | ECQ= | Periodic Health Check Event. Battery null, Temperature 18C.
| Device Reset Event | 17 | | CAISCHYxLjAuMC41 | Factory Reset Event, f/w Version "v1.0.0.5"
| Get Device Config | 18 | | CAIQtAEaCHYxLjAuMC41 | Gets the current configuration of Device 0.|
| Set Default Device Config | 19 |CAIQPA== | CAIQPBoIdjEuMC4wLjU= |  Sets the current configuration of Device 0.|
| Set Time Settings | 20 | GJxLILnAASgA | | Sets Time Settings to Latitude 48.138, Longitude -123.173, daylights savings off.  (note: Time zone is derived from lat/lon.)
| Get Time Settings | 21 | | EAAYnEsgucABKAA= | Gets the current time config information.
| Synchronize Time Request | 22 | IAE= | EN3MpJEG | Sync Time Request Device Time.  Example: Returned Epoch 1646863965
| Synchronize Time | 22 | CMiypJEGIAA= | | Sync time using "rough" Server Time Epoch: 1646860616 <br/>Note: Device reboots after time sync!
| Synchronize Time | 22 | CKi0pJEGEMiypJEGGAIgAA== | | Sync time using Server Time Epoch: 1646860840, Device Time Epoch: 1646860840, and Correction of 2 seconds <br/>Note: Device reboots after time sync!
| Schedule Get All | 26 | CAA= | (1) CAASCggBEAEYBCADKAA=<br/> (2) CAASEggCEAEYAiACK<br/>AFSBggeEB4YAg== <br/> (3) CAASHAgDEAEYASADKAM<br/>yBQjYARAuWgkQARh/IAAotAE= | Uplinks all Schedule 0 Schedule Items (example showing Schedule IdItem (1) (default toggle), (2) (trigger event), & (3) (weekly alarm))
| Schedule Set Item(s) | 25 | CAASEggCEAEYAiAC<br/>KAFSBggeEAEYAg== | | Sets Schedule 0 Schedule Item 2 to 30 second offset, then Bimodal Swich 0 to on (Set) for 30 seconds, then off (Unset).
| Schedule Get Item | 28 | CAAQAg== | CAASEggCEAEYAiA<br/>CKAFSBggeEB4YAg== | Gets Schedule 0 Schedule Item 2
| Schedule Trigger Item | 29 | CAAQAg== | Switch Notify Event* | Trigger Schedule 0 Schedule Item 2 <br/>*If Schedule Item 2 causes a Switch Notification Event, the response on Port 64 will be uplinked assuming Notifications are enabled in Bimodal Switch Config.
| Temp Trigger Event | 48 | | CAAQARhaIAI= | Temp Sensor 0, Triger Id 1 Event Notification.  Temperature 45C, Uncertainty 2C.
| Temp Report Event | 49 |  | CAAQHhgC | Temp Sensor 0 Report Event, Temperature 15C, uncertainty 2C
| Temp Get Config | 51 | CAA= | CAAYACAAKAAwATgA | Get Temp Sensor 0 configuration
| Temp Set Default Config | 52 | CAAYACAAKAAwATgA | CAAYACAAKAAwATgA | Set Temp Sensor 0 to factory default configuration|
| Temp Set Config | 52 | CAAYASAFKDwwATgA | CAAYASAFKDwwATgA | Configure Temp Sensor 0 to sample 1 minute interval, report stats 5 minute interval|
| Temp Set Config | 52 | CAASDAgBECEgACoECAAQAg== | CAASDggBECEYASAAKgQ<br/>IABACGAUgACg8MAE4AA== | Configure Temp Sensor 0 TriggerId 1 to Trigger Scheduler 0 Schedule ItemId 2 when absolute temperature is greater than (ABSGT) 33C.  (*note make sure temperature sampling is enabled)
| Temp Get Stats | 53 | CAA= | CAAQJBhaIDIosAEwCw== | Get Statistics from Temp Sensor 0
| Temp Stats Clear | 54 | CAA= | | Clears Temp Sensor 0 statistics|
| Switch Notify Event | 64 | | CAAQARgAKMMC | Switch 0 Set Event, Last State Duration 323 seconds|
| Switch Notify Event <br/> (Schedule Linked Trigger Example) | 64 | | CAAQARgCIAIoTw== | Switch 0 Set Event, Triggered by Scheduler ItemId 2, Action Set, Last State Duration 79 seconds.
| Switch Stats Event | 65 | | CAASCggHEMEDGAEg4wE<br/>aCggHEJALGAAgnAc= | Switch 0 statistics|
| Get Switch Config | 66 | CAA= | CAAQARgBIAAoAA | Get switch 0 configuration|
| Set Default Switch Config | 67 | CAAQARgBIAAoAA== | CAAQARgBIAAoAA==  | Sets Switch 0 to Factory Defaults|
| Set Switch Config | 67 | CAAQABgAIAUoAQ== | CAAQABgAIAUoAQ== | Sets Switch 0 to Report Stats every 5 minutes, disable event notifications|
| Switch Get State | 70 | CAA= | CAAQAQ== | Gets current state of Switch 0.  Switch is On (set).
| Switch Set State | 71 | CAAQAQ== | | Sets Switch 0 to On (set)
| Switch Set State | 71 | CAAQAA== | | Sets Switch 0 to Off (unset)
| Switch Set State | 71 | CAAQAg== | | Sets Switch 0 to toggle

## Integrator's Guide
This section is for integrator's adapting CS1030 devices to their application.  While the required set of messages that must be supported will vary from application to application.  This section outlines the minimum integration and minimum configuration capabilities for users to use the basic scheduling features.

### Minimum Integration
  Bare minimum integration requires support of the following messages.  These messages provide the required interaction to set time and receive device notifications.

| Port | Direction | Name | Description |
|---|:---:|:---:|---|
| 16 (0x10) | Uplink | Health Check Event | Periodic uplink from device indicating it is only.  Message will contain current reported temperature. |
| 17 (0x11) | Uplink | Reset Event | Message sent whenever the device is reset. Whenever this is received, the adaptor should send a synchronize time request message. |
| 22 (0x16) | Uplink/Downlink | Synchronize Time | Message sent / received by device to set time.   To support class A or (Helium) or clacc C deployments of the outlet,  the uplink request provides time offset information that can be provided on a subsequent downlink containing the time correction.  See discussion above discussing time synchronization. |
| 64 (0x40) | Uplink | Notify Event | Notifies adaptor of bimodal state changes.  Process this to be notified whenever the device switches on (SET) or off (UNSET) |                                                                            |
### Minimum Configuration Capability

For basic use of the device, users need to be able to configure the health check interval, set location or timezone, define one or more weekly schedule items, and be able to turn the device on or off. Advanced temperature functions are considered out of scope for basic used.  However, it recommended that user interfaces provide a means for the user to select a pred defined configuration will set the device of a known 'standard' configuration.  System's integrators will likely want this capability to readily specify the configuration of many CS1030 device at once.  

| Port | Direction | Name | Description |
|---|:---:|:---:|---|
| 18 (0x12) | Uplink/Downlink | Get Device Config | Device configuration request / response command allows retrieval of current device configuration.  Adaptor should save the configuration state once received. |
| 19 (0x13) | Downlink | Set Device Config | Sets the device configuration.  Use this to set the device configuration, minimum implementation will allow specification of the health check interval. |
| 20 (0x14) | Downlink | Set Time Settings | Sets time zone and/or location (latitude and longitude) of device.  Location provides the most precise sunrise/sunset calculation.   Optionally, daylight savings can be enabled or disabled. |
| 21 (0x15) | Downlink/Uplink | Get Time Settings | Gets time zone, current time, and location information of the device.  |
| 70 (0x46) | Downlink/Uplink | Get Switch State | Requests current state of switch.   |
| 71 (0x47) | Downlink/Uplink | Sets Switch State | Sets the switch state:  set, unset, or toggle. |
| 25 (0x19) | Downlink | Set Schedule | Sets one or more schedule items.  Minimum capability should support user specification of weekly schedule items. For simplicity downlink one message for each schedule item. |
| 26 (0x1A) | Downlink/Uplink | Get Schedule | Gets the current schedule for the device.  One message is uplinked for each defined schedule item. |
| 27 (0x1B) | Downlink | Reset Schedule | Resets the schedule, clearing to defaults. |

#### Weekly Scheduling
The CS1030 can support up to eight schedule items. The minimum configuration capability should allow users to specify weekly schedule items enabling them to turn on and off the device by a specific time or relative to 
sunrise or sunset.  Each schdeule item can be set for one or more days in the week, beginning on Sunday. See examples above for how to specify weekly schedule items.   

#### Selecting a Pre-Defined Configuration
To provide for advanced configurations not available through the user interface.  The application servers should provide a capability to specify in a template a set of predefined configurations, that are identiable by name and description.  Each configuration may contain one or more downlink messages specifying port and base64 payload data that are sent in the order listed.   Users can select these predefined configurations to quickly configure a device.   Ideally, additional predefined configurations can be added to the device template as required or needs change.   Need to consider how pre-defined configurations are assigned per integrator or customer.

For example,  a predefined configuration table may have a structure stored in the template as follows:

| Name | Description | Downlink Commands (description shown, actual {port, payload (base64)}) |
|---|---|---|
| **Attic Fan #1** | Turns on when ambient temperature > 29 C (\~85 F).<br>Turns off when temperature falls below 26 C (\~79 F). | 1. Reset Schedule<br>2. Schedule Item #1 Start Action (On)<br>3. Schedule Item #2 Start Action (Off)<br>4. Temperature Trigger 1 (ABSGT 29, link item 1)<br>5. Temperature Trigger 2 (ABSLT 26, link item 2) |
| **Street Lights #1** | Turns on 1/2 hour after sunset.<br>Turns off 1/2 before sunrise. | 1. Reset Schedule<br>2. Schedule Item #1 Weekly (On, 1800 secs offset sunset)<br>3. Schedule Item #2 Weekly (Off, -1800 secs offset sunrise)  |
| **Accent Lighting #1** | Turns on 1/2 hour before sunset Monday through Friday.<br>Turns off 8 hours later. | 1. Reset Schedule<br>2. Schedule Item #1 Weekly (On, -1800 secs offset sunset, M-F, link 2)<br>3. Schedule Item #2 Timer  (Off, delay 28800 seconds) |



## Firmware Errata Summary
| Issue # | Versions affected | Summary|
|:--------------:|:-----------:|:-------------|
| 1 | v1.0.7.2 and prior | TimeConfiguration timezone parameter does not work

### #1 TimeConfiguration timezone parameter
This issue is fixed in all firmware releases after v1.0.7.2.

All firmware releases up to and including v1.0.7.2 (2022/05/12) do not properly support the TimeConfiguration timezone parameter for setting UTC time offsets per ISO 8601.  Do not use the timezone parameter.

**Work Around**

Use latitude/longitude to set time zone.  If latitude/longitude is not available, longitude can be easily derived from timezone offset.  Examples are shown in C and Javascript below.  The functions calculate the approximate longitude in 1/100th of a degree given the time zone offset +-24hr.  The function ensures the range of the initial offset is +-24 hours and then calculates the approximate longitude.   

*C*
``` c
  /**
  * @brief Converts time zone to approximate longitude
  * @param tz Time zone offset in hours relatively to GMT.  Range is +/-24 hours.   
  *  Partial hours supported.
  * @returns  Returns the approximate longitude corresponding to the specified hour angle in 1/100th 
  *  of a degree. This is the same units expected by the TimeConfiguration protobuf message.
  */
  int tz2lon( float tz) 
  {
    int days = (int) (tz/24.0F);
    tz = tz - days*24;
    return (int) (tz * 100.0F * 360.0F / 24.0F);
  }
```

*Javascript [(click here for full example)](examples/js/ConvertTimeZone/convert.js)*
``` Javascript 

  /**
   * Converts time zone to approximate longitude
   * @param {number} tz Time zone offset in hours relatively to GMT.  Range is +/-24 hours.  
   *  Partial hours supported.
   * @returns {number} Returns the approximate longitude corresponding to the specified hour
   *  angle in 1/100th of a degree. This is the same units expected by the TimeConfiguration 
   *  protobuf message.
   */
  function tz2lon(tz) {
    let days = parseInt(tz / 24.0, 10);
    tz = tz - days * 24.0;
    return parseInt((tz * 100.0 * 360.0) / 24.0, 10);
  }

```

  *Matlab* 

  ``` Matlab
  function [ longitude] = tz2lon( timezone )
  %tz2lon Converts timezone into approximate integer longitude. 
  
    % Given times zone in hours, returns longituded in 1/100th of a degree
    %Bound the timezone to +- 24.0 hrs.
    days = fix(timezone/24);
    timezone = timezone - days*24;
    longitude = fix( timezone*100.0*360.0/24.0);
end


  ```


## License

Copyright 2022 Codepoint Technologies, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO 
EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
USE OR OTHER DEALINGS IN THE SOFTWARE.
