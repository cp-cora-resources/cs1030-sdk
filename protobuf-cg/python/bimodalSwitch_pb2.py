# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: bimodalSwitch.proto
"""Generated protocol buffer code."""
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


import common_pb2 as common__pb2
import scheduleTypes_pb2 as scheduleTypes__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x13\x62imodalSwitch.proto\x12\x12\x43ora.BimodalSwitch\x1a\x0c\x63ommon.proto\x1a\x13scheduleTypes.proto\"\x8d\x01\n\x0cNotification\x12\n\n\x02id\x18\x01 \x02(\r\x12*\n\x05\x65vent\x18\x02 \x02(\x0e\x32\x1b.Cora.BimodalSwitch.EventId\x12\x0e\n\x06idItem\x18\x03 \x02(\r\x12%\n\x06\x61\x63tion\x18\x04 \x01(\x0e\x32\x15.Cora.Schedule.Action\x12\x0e\n\x06\x64tLast\x18\x05 \x02(\r\"A\n\tStateInfo\x12\n\n\x02id\x18\x01 \x02(\r\x12(\n\x05state\x18\x02 \x02(\x0e\x32\x19.Cora.BimodalSwitch.State\"\x8b\x01\n\rConfiguration\x12\n\n\x02id\x18\x01 \x02(\r\x12\x1b\n\rconfirmNotify\x18\x02 \x01(\x08:\x04true\x12\x1a\n\x0c\x65nableNotify\x18\x03 \x01(\x08:\x04true\x12\x18\n\rstatsInterval\x18\x04 \x01(\r:\x01\x30\x12\x1b\n\x0c\x63onfirmStats\x18\x05 \x01(\x08:\x05\x66\x61lse\"X\n\nStateStats\x12\r\n\x05\x63ount\x18\x01 \x02(\r\x12\x11\n\ttotalTime\x18\x02 \x02(\r\x12\x13\n\x0bminDuration\x18\x03 \x02(\r\x12\x13\n\x0bmaxDuration\x18\x04 \x02(\r\"~\n\nStatistics\x12\n\n\x02id\x18\x01 \x02(\r\x12\x32\n\nstatsUnset\x18\x02 \x02(\x0b\x32\x1e.Cora.BimodalSwitch.StateStats\x12\x30\n\x08statsSet\x18\x03 \x02(\x0b\x32\x1e.Cora.BimodalSwitch.StateStats*)\n\x07\x45ventId\x12\x0f\n\x0b\x45VENT_UNSET\x10\x00\x12\r\n\tEVENT_SET\x10\x01*9\n\x05State\x12\x0f\n\x0bSTATE_UNSET\x10\x00\x12\r\n\tSTATE_SET\x10\x01\x12\x10\n\x0cSTATE_TOGGLE\x10\x02\x32\x88\x04\n\x06Switch\x12>\n\x0bNotifyEvent\x12 .Cora.BimodalSwitch.Notification\x1a\r.Cora.VoidMsg\x12\x41\n\x10StatsReportEvent\x12\x1e.Cora.BimodalSwitch.Statistics\x1a\r.Cora.VoidMsg\x12\x41\n\tGetConfig\x12\x11.Cora.Uint32Param\x1a!.Cora.BimodalSwitch.Configuration\x12Q\n\tSetConfig\x12!.Cora.BimodalSwitch.Configuration\x1a!.Cora.BimodalSwitch.Configuration\x12=\n\x08GetStats\x12\x11.Cora.Uint32Param\x1a\x1e.Cora.BimodalSwitch.Statistics\x12.\n\nStatsClear\x12\x11.Cora.Uint32Param\x1a\r.Cora.VoidMsg\x12<\n\x08GetState\x12\x11.Cora.Uint32Param\x1a\x1d.Cora.BimodalSwitch.StateInfo\x12\x38\n\x08SetState\x12\x1d.Cora.BimodalSwitch.StateInfo\x1a\r.Cora.VoidMsg')

_EVENTID = DESCRIPTOR.enum_types_by_name['EventId']
EventId = enum_type_wrapper.EnumTypeWrapper(_EVENTID)
_STATE = DESCRIPTOR.enum_types_by_name['State']
State = enum_type_wrapper.EnumTypeWrapper(_STATE)
EVENT_UNSET = 0
EVENT_SET = 1
STATE_UNSET = 0
STATE_SET = 1
STATE_TOGGLE = 2


_NOTIFICATION = DESCRIPTOR.message_types_by_name['Notification']
_STATEINFO = DESCRIPTOR.message_types_by_name['StateInfo']
_CONFIGURATION = DESCRIPTOR.message_types_by_name['Configuration']
_STATESTATS = DESCRIPTOR.message_types_by_name['StateStats']
_STATISTICS = DESCRIPTOR.message_types_by_name['Statistics']
Notification = _reflection.GeneratedProtocolMessageType('Notification', (_message.Message,), {
  'DESCRIPTOR' : _NOTIFICATION,
  '__module__' : 'bimodalSwitch_pb2'
  # @@protoc_insertion_point(class_scope:Cora.BimodalSwitch.Notification)
  })
_sym_db.RegisterMessage(Notification)

StateInfo = _reflection.GeneratedProtocolMessageType('StateInfo', (_message.Message,), {
  'DESCRIPTOR' : _STATEINFO,
  '__module__' : 'bimodalSwitch_pb2'
  # @@protoc_insertion_point(class_scope:Cora.BimodalSwitch.StateInfo)
  })
_sym_db.RegisterMessage(StateInfo)

Configuration = _reflection.GeneratedProtocolMessageType('Configuration', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGURATION,
  '__module__' : 'bimodalSwitch_pb2'
  # @@protoc_insertion_point(class_scope:Cora.BimodalSwitch.Configuration)
  })
_sym_db.RegisterMessage(Configuration)

StateStats = _reflection.GeneratedProtocolMessageType('StateStats', (_message.Message,), {
  'DESCRIPTOR' : _STATESTATS,
  '__module__' : 'bimodalSwitch_pb2'
  # @@protoc_insertion_point(class_scope:Cora.BimodalSwitch.StateStats)
  })
_sym_db.RegisterMessage(StateStats)

Statistics = _reflection.GeneratedProtocolMessageType('Statistics', (_message.Message,), {
  'DESCRIPTOR' : _STATISTICS,
  '__module__' : 'bimodalSwitch_pb2'
  # @@protoc_insertion_point(class_scope:Cora.BimodalSwitch.Statistics)
  })
_sym_db.RegisterMessage(Statistics)

_SWITCH = DESCRIPTOR.services_by_name['Switch']
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _EVENTID._serialized_start=649
  _EVENTID._serialized_end=690
  _STATE._serialized_start=692
  _STATE._serialized_end=749
  _NOTIFICATION._serialized_start=79
  _NOTIFICATION._serialized_end=220
  _STATEINFO._serialized_start=222
  _STATEINFO._serialized_end=287
  _CONFIGURATION._serialized_start=290
  _CONFIGURATION._serialized_end=429
  _STATESTATS._serialized_start=431
  _STATESTATS._serialized_end=519
  _STATISTICS._serialized_start=521
  _STATISTICS._serialized_end=647
  _SWITCH._serialized_start=752
  _SWITCH._serialized_end=1272
# @@protoc_insertion_point(module_scope)
