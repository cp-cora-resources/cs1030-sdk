// source: bimodalSwitch.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Cora.BimodalSwitch.State');

/**
 * @enum {number}
 */
proto.Cora.BimodalSwitch.State = {
  STATE_UNSET: 0,
  STATE_SET: 1,
  STATE_TOGGLE: 2
};

