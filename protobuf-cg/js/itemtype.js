// source: scheduleTypes.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Cora.Schedule.ItemType');

/**
 * @enum {number}
 */
proto.Cora.Schedule.ItemType = {
  ITEMTYPE_UNKNOWN: 0,
  ITEMTYPE_WEEKLY: 1,
  ITEMTYPE_TIMER: 2,
  ITEMTYPE_TRIGGERSET: 3,
  ITEMTYPE_STARTACTION: 4
};

