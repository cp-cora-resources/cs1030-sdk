// source: scheduleTypes.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Cora.Schedule.Action');

/**
 * @enum {number}
 */
proto.Cora.Schedule.Action = {
  ACTION_NONE: 0,
  ACTION_UNSET: 1,
  ACTION_SET: 2,
  ACTION_TOGGLE: 3
};

