// source: temperature.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Cora.Temperature.TriggerMode');

/**
 * @enum {number}
 */
proto.Cora.Temperature.TriggerMode = {
  ABSGT: 0,
  ABSLT: 1,
  DELTA: 2,
  DELTAPOS: 3,
  DELTANEG: 4
};

