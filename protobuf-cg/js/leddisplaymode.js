// source: common.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Cora.LedDisplayMode');

/**
 * @enum {number}
 */
proto.Cora.LedDisplayMode = {
  DISABLED: 0,
  TELEMETRY: 1,
  ALL: 2
};

