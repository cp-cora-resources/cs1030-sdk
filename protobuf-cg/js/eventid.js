// source: bimodalSwitch.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Cora.BimodalSwitch.EventId');

/**
 * @enum {number}
 */
proto.Cora.BimodalSwitch.EventId = {
  EVENT_UNSET: 0,
  EVENT_SET: 1
};

