// source: scheduleTypes.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Cora.Schedule.TimerConfig');

goog.require('jspb.BinaryReader');
goog.require('jspb.BinaryWriter');
goog.require('jspb.Message');

/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.Cora.Schedule.TimerConfig = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.Cora.Schedule.TimerConfig, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.Cora.Schedule.TimerConfig.displayName = 'proto.Cora.Schedule.TimerConfig';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.Cora.Schedule.TimerConfig.prototype.toObject = function(opt_includeInstance) {
  return proto.Cora.Schedule.TimerConfig.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.Cora.Schedule.TimerConfig} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.Cora.Schedule.TimerConfig.toObject = function(includeInstance, msg) {
  var f, obj = {
    duration: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    delay: jspb.Message.getFieldWithDefault(msg, 2, 0),
    cyclecount: jspb.Message.getFieldWithDefault(msg, 3, 1)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.Cora.Schedule.TimerConfig}
 */
proto.Cora.Schedule.TimerConfig.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.Cora.Schedule.TimerConfig;
  return proto.Cora.Schedule.TimerConfig.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.Cora.Schedule.TimerConfig} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.Cora.Schedule.TimerConfig}
 */
proto.Cora.Schedule.TimerConfig.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setDuration(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setDelay(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setCyclecount(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.Cora.Schedule.TimerConfig.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.Cora.Schedule.TimerConfig.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.Cora.Schedule.TimerConfig} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.Cora.Schedule.TimerConfig.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeUint32(
      1,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 2));
  if (f != null) {
    writer.writeUint32(
      2,
      f
    );
  }
  f = /** @type {number} */ (jspb.Message.getField(message, 3));
  if (f != null) {
    writer.writeUint32(
      3,
      f
    );
  }
};


/**
 * required uint32 duration = 1;
 * @return {number}
 */
proto.Cora.Schedule.TimerConfig.prototype.getDuration = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.Cora.Schedule.TimerConfig} returns this
 */
proto.Cora.Schedule.TimerConfig.prototype.setDuration = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.Cora.Schedule.TimerConfig} returns this
 */
proto.Cora.Schedule.TimerConfig.prototype.clearDuration = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.Cora.Schedule.TimerConfig.prototype.hasDuration = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional uint32 delay = 2;
 * @return {number}
 */
proto.Cora.Schedule.TimerConfig.prototype.getDelay = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.Cora.Schedule.TimerConfig} returns this
 */
proto.Cora.Schedule.TimerConfig.prototype.setDelay = function(value) {
  return jspb.Message.setField(this, 2, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.Cora.Schedule.TimerConfig} returns this
 */
proto.Cora.Schedule.TimerConfig.prototype.clearDelay = function() {
  return jspb.Message.setField(this, 2, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.Cora.Schedule.TimerConfig.prototype.hasDelay = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional uint32 cycleCount = 3;
 * @return {number}
 */
proto.Cora.Schedule.TimerConfig.prototype.getCyclecount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 1));
};


/**
 * @param {number} value
 * @return {!proto.Cora.Schedule.TimerConfig} returns this
 */
proto.Cora.Schedule.TimerConfig.prototype.setCyclecount = function(value) {
  return jspb.Message.setField(this, 3, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.Cora.Schedule.TimerConfig} returns this
 */
proto.Cora.Schedule.TimerConfig.prototype.clearCyclecount = function() {
  return jspb.Message.setField(this, 3, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.Cora.Schedule.TimerConfig.prototype.hasCyclecount = function() {
  return jspb.Message.getField(this, 3) != null;
};


