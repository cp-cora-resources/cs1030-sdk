// source: scheduleTypes.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Cora.Schedule.Configuration');

goog.require('jspb.BinaryReader');
goog.require('jspb.BinaryWriter');
goog.require('jspb.Message');
goog.require('proto.Cora.Schedule.ScheduleItem');

/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.Cora.Schedule.Configuration = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.Cora.Schedule.Configuration.repeatedFields_, null);
};
goog.inherits(proto.Cora.Schedule.Configuration, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.Cora.Schedule.Configuration.displayName = 'proto.Cora.Schedule.Configuration';
}

/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.Cora.Schedule.Configuration.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.Cora.Schedule.Configuration.prototype.toObject = function(opt_includeInstance) {
  return proto.Cora.Schedule.Configuration.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.Cora.Schedule.Configuration} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.Cora.Schedule.Configuration.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: (f = jspb.Message.getField(msg, 1)) == null ? undefined : f,
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.Cora.Schedule.ScheduleItem.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.Cora.Schedule.Configuration}
 */
proto.Cora.Schedule.Configuration.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.Cora.Schedule.Configuration;
  return proto.Cora.Schedule.Configuration.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.Cora.Schedule.Configuration} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.Cora.Schedule.Configuration}
 */
proto.Cora.Schedule.Configuration.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setId(value);
      break;
    case 2:
      var value = new proto.Cora.Schedule.ScheduleItem;
      reader.readMessage(value,proto.Cora.Schedule.ScheduleItem.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.Cora.Schedule.Configuration.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.Cora.Schedule.Configuration.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.Cora.Schedule.Configuration} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.Cora.Schedule.Configuration.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = /** @type {number} */ (jspb.Message.getField(message, 1));
  if (f != null) {
    writer.writeUint32(
      1,
      f
    );
  }
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.Cora.Schedule.ScheduleItem.serializeBinaryToWriter
    );
  }
};


/**
 * required uint32 id = 1;
 * @return {number}
 */
proto.Cora.Schedule.Configuration.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.Cora.Schedule.Configuration} returns this
 */
proto.Cora.Schedule.Configuration.prototype.setId = function(value) {
  return jspb.Message.setField(this, 1, value);
};


/**
 * Clears the field making it undefined.
 * @return {!proto.Cora.Schedule.Configuration} returns this
 */
proto.Cora.Schedule.Configuration.prototype.clearId = function() {
  return jspb.Message.setField(this, 1, undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.Cora.Schedule.Configuration.prototype.hasId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated ScheduleItem items = 2;
 * @return {!Array<!proto.Cora.Schedule.ScheduleItem>}
 */
proto.Cora.Schedule.Configuration.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.Cora.Schedule.ScheduleItem>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.Cora.Schedule.ScheduleItem, 2));
};


/**
 * @param {!Array<!proto.Cora.Schedule.ScheduleItem>} value
 * @return {!proto.Cora.Schedule.Configuration} returns this
*/
proto.Cora.Schedule.Configuration.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.Cora.Schedule.ScheduleItem=} opt_value
 * @param {number=} opt_index
 * @return {!proto.Cora.Schedule.ScheduleItem}
 */
proto.Cora.Schedule.Configuration.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.Cora.Schedule.ScheduleItem, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.Cora.Schedule.Configuration} returns this
 */
proto.Cora.Schedule.Configuration.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};


