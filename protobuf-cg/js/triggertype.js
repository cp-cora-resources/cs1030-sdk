// source: scheduleTypes.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Cora.Schedule.TriggerType');

/**
 * @enum {number}
 */
proto.Cora.Schedule.TriggerType = {
  TRIGGER_EPOCH: 1,
  TRIGGER_SUNRISE: 2,
  TRIGGER_SUNSET: 3
};

