// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: cs1030_ul.proto

public final class Cs1030Ul {
  private Cs1030Ul() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\017cs1030_ul.proto\032\023poweredDevice.proto\032\023" +
      "bimodalSwitch.proto\032\021temperature.proto"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          Cora.Powered.PoweredDevice.getDescriptor(),
          Cora.BimodalSwitch.BimodalSwitch.getDescriptor(),
          Cora.Temperature.Temperature.getDescriptor(),
        });
    Cora.Powered.PoweredDevice.getDescriptor();
    Cora.BimodalSwitch.BimodalSwitch.getDescriptor();
    Cora.Temperature.Temperature.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
